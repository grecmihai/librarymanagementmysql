#creare tabele

-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema Biblioteca
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Biblioteca
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Biblioteca` DEFAULT CHARACTER SET utf8 ;
USE `Biblioteca` ;

-- -----------------------------------------------------
-- Table `Biblioteca`.`Membru`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Biblioteca`.`Membru` (
  `idMembru` INT NOT NULL AUTO_INCREMENT,
  `Nume` VARCHAR(20) NOT NULL,
  `Prenume` VARCHAR(20) NOT NULL,
  `Email` VARCHAR(25) NOT NULL,
  `NrTelefon` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`idMembru`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Biblioteca`.`Carti`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Biblioteca`.`Carti` (
  `Isbn` VARCHAR(30) NOT NULL,
  `Titlu` VARCHAR(45) NOT NULL,
  `Editura` VARCHAR(45) NULL,
  `AnAparitie` INT NULL,
  `NrPagini` INT NULL,
  `NrCopiiDisponibile` INT NOT NULL,
  `Limba` VARCHAR(15) NOT NULL,
  `Cod` DECIMAL(6,3) NOT NULL,
  PRIMARY KEY (`Isbn`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Biblioteca`.`ImprumuturiCurente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Biblioteca`.`ImprumuturiCurente` (
  `idMembru` INT NOT NULL,
  `Isbn` VARCHAR(30) NOT NULL,
  `DataImprumutului` DATE NOT NULL,
  `DataValabilitate` DATE NOT NULL,
  PRIMARY KEY (`idMembru`, `Isbn`),
  INDEX `Isbn_idx` (`Isbn` ASC),
  CONSTRAINT `idMembru`
    FOREIGN KEY (`idMembru`)
    REFERENCES `Biblioteca`.`Membru` (`idMembru`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Isbn`
    FOREIGN KEY (`Isbn`)
    REFERENCES `Biblioteca`.`Carti` (`Isbn`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Biblioteca`.`RezervariInternet`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Biblioteca`.`RezervariInternet` (
  `idMembru` INT NOT NULL,
  `Isbn` VARCHAR(30) NOT NULL,
  `DataRezervarii` DATE NOT NULL,
  `DataValabilitate` DATE NOT NULL,
  PRIMARY KEY (`idMembru`, `Isbn`),
  INDEX `Isbn_idx` (`Isbn` ASC),
  CONSTRAINT `idMembru1`
    FOREIGN KEY (`idMembru`)
    REFERENCES `Biblioteca`.`Membru` (`idMembru`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Isbn1`
    FOREIGN KEY (`Isbn`)
    REFERENCES `Biblioteca`.`Carti` (`Isbn`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Biblioteca`.`IstoricImprumuturi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Biblioteca`.`IstoricImprumuturi` (
  `idMembru` INT NOT NULL,
  `DataImprumutului` DATE NOT NULL,
  `DataReturnarii` DATE NOT NULL,
  `Isbn` VARCHAR(30) NOT NULL,
  INDEX `idMembru2_idx` (`idMembru` ASC),
  INDEX `vcs_idx` (`Isbn` ASC),
  CONSTRAINT `idMembru2`
    FOREIGN KEY (`idMembru`)
    REFERENCES `Biblioteca`.`Membru` (`idMembru`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `vcs`
    FOREIGN KEY (`Isbn`)
    REFERENCES `Biblioteca`.`Carti` (`Isbn`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Biblioteca`.`Penalizari`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Biblioteca`.`Penalizari` (
  `idMembru` INT NOT NULL AUTO_INCREMENT,
  `NrZileIntarziate` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`idMembru`),
  CONSTRAINT `casca`
    FOREIGN KEY (`idMembru`)
    REFERENCES `Biblioteca`.`Membru` (`idMembru`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `Biblioteca`.`Adresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Biblioteca`.`Adresa` (
  `idMembru` INT NOT NULL,
  `Strada` VARCHAR(20) NOT NULL,
  `Numar` INT NOT NULL,
  `Bloc` VARCHAR(5) NULL,
  `Apartament` INT NULL,
  PRIMARY KEY (`idMembru`),
  CONSTRAINT `idMembru3`
    FOREIGN KEY (`idMembru`)
    REFERENCES `Biblioteca`.`Membru` (`idMembru`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Biblioteca`.`Autor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Biblioteca`.`Autor` (
  `idAutor` INT NOT NULL AUTO_INCREMENT,
  `Nume` VARCHAR(45) NOT NULL,
  `Prenume` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idAutor`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Biblioteca`.`Gen`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Biblioteca`.`Gen` (
  `idGen` INT NOT NULL AUTO_INCREMENT,
  `Gencol` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`idGen`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Biblioteca`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Biblioteca`.`User` (
  `idUser` INT NOT NULL AUTO_INCREMENT,
  `Cont` VARCHAR(45) NOT NULL,
  `Parola` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`idUser`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Biblioteca`.`Carti_Autor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Biblioteca`.`Carti_Autor` (
  `Isbn` VARCHAR(30) NOT NULL,
  `idAutor` INT NOT NULL,
  INDEX `cheiestr_idx` (`Isbn` ASC),
  INDEX `cheiestr2_idx` (`idAutor` ASC),
  PRIMARY KEY (`Isbn`, `idAutor`),
  CONSTRAINT `cheiestr`
    FOREIGN KEY (`Isbn`)
    REFERENCES `Biblioteca`.`Carti` (`Isbn`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `cheiestr2`
    FOREIGN KEY (`idAutor`)
    REFERENCES `Biblioteca`.`Autor` (`idAutor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Biblioteca`.`Carti_Gen`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Biblioteca`.`Carti_Gen` (
  `Isbn` VARCHAR(30) NOT NULL,
  `idGen` INT NOT NULL,
  INDEX `cheie1_idx` (`Isbn` ASC),
  INDEX `cheie2_idx` (`idGen` ASC),
  PRIMARY KEY (`Isbn`, `idGen`),
  CONSTRAINT `cheie1`
    FOREIGN KEY (`Isbn`)
    REFERENCES `Biblioteca`.`Carti` (`Isbn`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `cheie2`
    FOREIGN KEY (`idGen`)
    REFERENCES `Biblioteca`.`Gen` (`idGen`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Biblioteca`.`Cod_Carti`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Biblioteca`.`Cod_Carti` (
  `Cod` DECIMAL(6,3) NOT NULL,
  `Descriere` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Cod`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Biblioteca`.`ValiditateImprumut`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Biblioteca`.`ValiditateImprumut` (
  `idMembru` INT NOT NULL,
  `dreptImprumut` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idMembru`),
  CONSTRAINT `lihggf`
    FOREIGN KEY (`idMembru`)
    REFERENCES `Biblioteca`.`Membru` (`idMembru`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
