#interogari

#1 Numele si Prenumele membrilor care au gmail

select nume , prenume from membru where email like '%gmail.com';

#2 Numarul genurilor

select count(gencol) as nrGenuri from gen;

#3 Numele si prenumele persoanelor cu mai mult de o carte imprumutate 

select nume , prenume , count(imprumuturicurente.idMembru) as nrCartiImprumutate 
from membru join imprumuturicurente on membru.idMembru = imprumuturicurente.idMembru
group by imprumuturicurente.idMembru having nrCartiImprumutate > 1;

#4 Numele si Prenumele persoanelor care au imprumutat carti scrise de Lehane
select membru.Nume , membru.Prenume from membru
join istoricimprumuturi on membru.idMembru = istoricimprumuturi.idMembru
join carti_autor on istoricimprumuturi.Isbn = carti_autor.Isbn
join autor on autor.idAutor = carti_autor.idAutor
where autor.Nume = 'Lehane';

#5Numarul de pagini ale cartilor scrise de autorul cu id=1

select sum(nrpagini) as nrPaginiTotal from carti
join carti_autor on carti.Isbn = carti_autor.Isbn
join autor on carti_autor.idAutor = autor.idAutor
where carti_autor.idAutor = 1;

#6 titlul si editura cartilor aparute dupa 2009
select titlu , editura from carti where anaparitie>2009;

#7  Membrii care sunt in reteaua Orange
select nume , prenume from membru where nrtelefon like '074%' or nrtelefon like '075%';

#8 perechile de nume ale membrilor ce locuiesc pe aceeasi strada

select distinct  a.Nume , b.Nume from adresa as x
join adresa as y on x.Strada = y.Strada and x.idMembru < y.idMembru 
join membru as a on a.idMembru = x.idMembru
join membru as b on b.idMembru = y.idMembru;

#9 Numele si prenumele membrului cu cele mai multe carti imprumutate

select nume , prenume , count(istoricimprumuturi.idMembru) as nrTotalCarti from membru
join istoricimprumuturi on istoricimprumuturi.idMembru = membru.idMembru
group by istoricimprumuturi.idMembru order by nrTotalCarti desc limit 1;

#10 Editurile
select distinct Editura from carti;

#11 Cartile care au aparut intre 2005 si 2009 si au mai putin de 3 copii disponibile
select titlu from carti where carti.AnAparitie between 2005 and 2009 and carti.NrCopiiDisponibile < 3;

#12 Numele si prenumele persoanelor care au imprumutat carti in august
select distinct nume , prenume from membru
join istoricimprumuturi on istoricimprumuturi.idMembru = membru.idMembru
where month(istoricimprumuturi.DataImprumutului) = 8;

#13 Numele si prenumele persoanelor care au imprumutat carti in prima jumatate a lunii
select distinct nume , prenume from membru
join istoricimprumuturi on istoricimprumuturi.idMembru = membru.idMembru
where day(istoricimprumuturi.DataImprumutului) between 1 and 15;

#14 Numele si prenumele persoanelor care au prelungit carti
select distinct nume , prenume from membru
join istoricimprumuturi on istoricimprumuturi.idMembru = membru.idMembru
where ((day(istoricimprumuturi.DataReturnarii) - day(istoricimprumuturi.DataImprumutului)) > 14 
and month(istoricimprumuturi.DataImprumutului) = month(istoricimprumuturi.DataReturnarii))
or ((30 + day(istoricimprumuturi.DataReturnarii) - day(istoricimprumuturi.DataImprumutului)) > 14
and month(istoricimprumuturi.DataImprumutului) <> month(istoricimprumuturi.DataReturnarii));

#15 Numele si prenumele membrilor ce au imprumutat carti din literatura germanica
select distinct membru.nume , membru.prenume from membru
join istoricimprumuturi on istoricimprumuturi.idMembru = membru.idMembru
join carti on carti.Isbn = istoricimprumuturi.Isbn
join cod_carti on cod_carti.Cod = carti.Cod
where cod_carti.Descriere like '%germanice';