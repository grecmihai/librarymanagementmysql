#triggere
#1 cand se adauga o carte in baza de date , ea sa fie adaugata si in vederea Carte
drop trigger if exists after_carti_insert;
delimiter //
create trigger after_carti_insert after insert on carti for each row
begin
declare numeAutor varchar(45);
declare prenumeAutor varchar(45);
declare genCarte varchar(15);
declare descriereCarte varchar(45);

select nume , prenume into numeAutor , prenumeAutor from autor 
join carti_autor on autor.idAutor = carti_autor.idAutor
where carti_autor.isbn = new.Isbn;

select gen.Gencol into genCarte from gen
join carti_gen on gen.idGen = carti_gen.idGen
where carti_gen.Isbn = new.Isbn;

select descriere into descriereCarte from cod_carti where cod_carti.Cod = new.Cod;

insert into carte values (new.Titlu , numeAutor , prenumeAutor , genCarte , descriereCarte);
end//
delimiter ;

#2 Cand se sterge o carte din baza de date , se sterge automat si din vederea Carte si se rup legaturile din Carti-Autor si Carti-Gen
#de asemenea se sterge si din istoric imprumuturi deoarece cartea nu mai exista in baza de date , deci s'ar genera o eroare la
#procedura de afisare a cartilor imprumutate de'a lungul timpului
drop trigger if exists after_carti_delete;
delimiter //
create trigger after_carti_delete after delete on carti for each row
begin
delete from carte where carte.titlu = old.Titlu;
delete from carti_autor where carti_autor.Isbn = old.Isbn;
delete from carti_gen where carti_gen.Isbn = old.Isbn;
delete from istoricimprumuturi where istoricimprumuturi.Isbn = old.Isbn;
delete from rezervariinternet where rezervariinternet.Isbn = old.Isbn;
end//
delimiter ;
#3 Vom crea 2 triggere pt cand se updateaza membru sau adresa , pt a se updata si vederea membrucomplet
drop trigger if exists after_membru_update;
delimiter //
create trigger after_membru_update after update on membru for each row
begin
update membrucomplet set membrucomplet.Email = new.Email where membrucomplet.Nume = old.Nume and membrucomplet.Prenume = old.Prenume;
update membrucomplet set membrucomplet.NrTelefon = new.NrTelefon where membrucomplet.Nume = old.Nume and membrucomplet.Prenume = old.Prenume;
end//
delimiter ;

drop trigger if exists after_adresa_update;
delimiter //
create trigger after_adresa_update after update on adresa for each row
begin
update membrucomplet join membru on membrucomplet.Nume = membru.Nume and membrucomplet.Prenume = membru.Prenume
set membrucomplet.Strada = new.Strada where membru.idMembru = old.idMembru; 
update membrucomplet join membru on membrucomplet.Nume = membru.Nume and membrucomplet.Prenume = membru.Prenume
set membrucomplet.Numar = new.Numar where membru.idMembru = old.idMembru; 
update membrucomplet join membru on membrucomplet.Nume = membru.Nume and membrucomplet.Prenume = membru.Prenume
set membrucomplet.Bloc = new.Bloc where membru.idMembru = old.idMembru; 
update membrucomplet join membru on membrucomplet.Nume = membru.Nume and membrucomplet.Prenume = membru.Prenume
set membrucomplet.Apartament = new.Apartament where membru.idMembru = old.idMembru; 
end//
delimiter ;

show triggers;