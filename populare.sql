#umplere tabele

insert into Autor (Nume , Prenume) values
('Patterson' , 'James') ,
('Haruki' , 'Murakami') , 
('Lehane' , 'Dennis') , 
('Dickens' , 'Charles') ,
('Tolstoi' , 'Lev');

insert into Autor(Nume , Prenume) values ('Larsson' , 'Stieg');
insert into autor(nume , prenume) values ('Dostoievski' , 'Feodor');
insert into autor (nume , prenume) values ('Coelho' , 'Paolo');
insert into autor (nume , prenume) values ('Hemingway' , 'Ernest');
insert into autor (nume , prenume) values ('Martin' , 'George');
insert into autor (nume , prenume) values ('Rowling' , 'J.K.');
insert into autor (nume , prenume) values ('Eminescu' , 'Mihai') ,
('Stanescu' , 'Nichita') ,
('Caragiale' , 'Ion Luca');


insert into Gen (Gencol) values
('Thriller') , 
('Poezie') , 
('Romantic') ,
('Crime') , 
('Teatru') ,
('Drama');

insert into Membru (idMembru , Nume , Prenume , Email , NrTelefon) values 
(1001 , 'Grec' , 'Mihai' , 'grec_mihai96@yahoo.com' , '0751291412');

insert into Membru (Nume , Prenume , Email , NrTelefon) values
('Belascu' , 'Dorin' , 'dodo96@gmail.com' , '0746201241') , 
('Supuran' , 'Marius' , 'mariussupuran@yahoo.com' , '0755019212') , 
('Palade' , 'Tudor' , 'tudorpalade2006@yahoo.com' , '0724012412') , 
('Ghiurco' , 'Iulia' , 'iuliagc@gmail.com' , '0766019429') , 
('Serban' , 'Iulia' , 'iuliaserban@yahoo.com' , '0754912400') ,
('Curtean' , 'Maria' , 'mariacurtean97@gmail.com' , '0761009120') ,
('Marinscas' , 'Oana' , 'oana.marincas@gmail.com' , '0748909821');

insert into Adresa values (1001 , 'Pictor Ioan Sima' , 8 , 'P27' , 3);
insert into Adresa  values
(1002 , 'Sperantei' , 10 , 'D11' , 1) , 
(1003 , 'Pietris' , 2 , 'C3' , 15) ,
(1004 , 'Mihai Viteazul' , 4 , 'E22' , 9) ,
(1005 , 'Sfanta Vineri' , 12 , null , null) , 
(1006 , 'Loc Col Pretorian' , 12 , 'C7' , 13) , 
(1007 , 'Mihai Viteazul' , 8 , 'E39' , 3) ,
(1008 , 'Pictor Ioan Sima' , 13 , 'P38' , 12);

insert into Carti values 
('973-629-096-4' , 'Alerta de gradul zero' , 'Lider' , 2006 , 327 , 2 , 'Romana' , 821.111 ) ,
('978-973-54-0068-2' , 'Bikini' , 'RAO' , 2009 , 346 , 2 , 'Romana' , 821.111 ) , 
('973-629-079-4' , 'Bufonul' , 'Lider' , 2005 , 416 , 3 , 'Romana' , 821.111) , 
('978-973-103-669-4' , 'Casa de pe plaja' , 'RAO' , 2008 , 346 , 2 , 'Romana' , 821.111) , 
('978-973-46-1945-0' , 'Padurea norvegiana' , 'Polirom' , 2011 , 359 , 3 , 'Romana' , 821.17) ,
('978-973-47-1246-5' , 'Disparuta fara urma' , 'Paralela 45' , 2011 , 464 , 3 , 'Romana' , 821.111) , 
('973-675-111-2' , 'Misterele fluviului' , 'Litera international' , 2004 , 524 , 3 , 'Romana' , 821.111) , 
('978-973-47-0649-5' , 'Shutter Island' , 'Paralela 45' , 2009 , 347 , 3 , 'Romana' , 821.111) , 
('973-9293-13-1' , 'Aventurile lui Oliver Twist' , 'Allfa' , 1997 , 476 , 3 , 'Romana' , 821.111) , 
('978-606-539-760-6' , 'David Copperfield' , 'Adevarul Holding' , 2011 , 1024 , 5 , 'Romana' , 821.111) ,
('978-973-46-0662-7' , 'Anna Karenina' , 'Polirom' , 2007 , 903 , 5 , 'Romana' , 821.16) , 
('978-973-707-220-7' , 'Barbati care urasc femeile' , 'Editura Trei' , 2008 , 684 , 4 , 'Romana' , 821.11) , 
('978-973-707-304-4' , 'Castelul din nori s-a sfaramat' , 'Editura Trei' , 2009 , 853 , 4 , 'Romana' , 821.11) , 
('978-973-707-263-4' , 'Fata care s-a jucat cu focul' , 'Editura Trei' , 2009 , 757 , 4 , 'Romana' , 821.11);

insert into Carti_Autor (Isbn , idAutor) values
('973-629-096-4' , 1) , 
('978-973-54-0068-2' , 1) ,
('973-629-079-4' , 1) ,
('978-973-103-669-4' , 1) ,
('978-973-46-1945-0' , 2) ,
('978-973-47-1246-5' , 3) ,
('973-675-111-2' , 3) ,
('978-973-47-0649-5' , 3) ,
('973-9293-13-1' , 4) ,
('978-606-539-760-6' , 4) ,
('978-973-46-0662-7' , 5) ,
('978-973-707-220-7' , 6) ,
('978-973-707-304-4' , 6) ,
('978-973-707-263-4' , 6);

insert into carti_gen (Isbn , idGen) values
('973-629-096-4' , 1) , 
('978-973-54-0068-2' , 1) ,
('973-629-079-4' , 1) ,
('978-973-103-669-4' , 1) ,
('978-973-46-1945-0' , 6) ,
('978-973-47-1246-5' , 1) ,
('973-675-111-2' , 1) ,
('978-973-47-0649-5' , 1) ,
('973-9293-13-1' , 6) ,
('978-606-539-760-6' , 6) ,
('978-973-46-0662-7' , 6) ,
('978-973-707-220-7' , 4) ,
('978-973-707-304-4' , 4) ,
('978-973-707-263-4' , 4);

insert into ImprumuturiCurente (idMembru , Isbn , DataImprumutului , DataValabilitate) values 
(1001 , '978-973-46-1945-0' , '2016-12-11' , '2016-12-25') ,
(1002 , '978-973-46-0662-7' , '2016-12-02' , '2016-12-16') ,
(1003 , '978-973-47-1246-5' , '2016-12-08' , '2016-12-22') ,
(1003 , '973-675-111-2' , '2016-12-08' , '2016-12-22') ,
(1003 , '978-973-47-0649-5' , '2016-12-08' , '2016-12-22') ,
(1005 , '978-606-539-760-6' , '2016-12-12' , '2016-12-20') ,
(1008 , '978-973-46-1945-0' , '2016-12-11' , '2016-12-25');


insert into RezervariInternet values
(1004 , '978-973-707-220-7' , '2016-12-12' , '2016-12-15') , 
(1006 , '978-973-46-1945-0' , '2016-12-13' , '2016-12-16');

insert into IstoricImprumuturi (idMembru , Isbn , DataImprumutului , DataReturnarii) values
(1001 , '978-973-707-220-7' , '2016-07-14' , '2016-07-24') ,
(1001 , '978-973-707-304-4' , '2016-07-24' , '2016-08-08') ,
(1001 , '978-973-707-263-4' , '2016-08-19' , '2016-08-22') ,
(1002 , '978-973-54-0068-2' , '2016-07-19' , '2016-07-30') ,
(1002 , '978-606-539-760-6' , '2016-08-14' , '2016-08-15') ,
(1004 , '978-973-47-0649-5' , '2016-09-09' , '2016-09-18');

insert into ValiditateImprumut (idMembru) values 
(1001) ,
(1002) ,
(1003) ,
(1004) ,
(1005) ,
(1006) ,
(1007) ,
(1008);

insert into Cod_carti values
(821.111 , 'Literatura engleza si de limba engleza') , 
(821.17 , 'Alte literaturi') ,
(821.16 , 'Literaturi slave') ,
(821.11 , 'Literaturi de limbi germanice');

insert into IstoricImprumuturi (idMembru , Isbn , DataImprumutului , DataReturnarii) values
(1001 , '978-973-46-1945-0' , '2016-12-11' , '2016-12-25') ,
(1002 , '978-973-46-0662-7' , '2016-12-02' , '2016-12-16') ,
(1003 , '978-973-47-1246-5' , '2016-12-08' , '2016-12-22') ,
(1003 , '973-675-111-2' , '2016-12-08' , '2016-12-22') ,
(1003 , '978-973-47-0649-5' , '2016-12-08' , '2016-12-22') ,
(1005 , '978-606-539-760-6' , '2016-12-12' , '2016-12-20') ,
(1008 , '978-973-46-1945-0' , '2016-12-11' , '2016-12-25');

insert into penalizari (idMembru) values 
(1001) ,
(1002) ,
(1003) ,
(1004) ,
(1005) ,
(1006) ,
(1007) ,
(1008);

insert into gen(gencol) values ('Science Fiction') , ('Fantasy') , ('Aventura');

insert into carti  values
('973-98761-1-0' , 'Adolescentul' , 'RAO' , 1998 , 704 , 5 , 'Romana' , 821.16) , 
('978-973-104-069-1' , 'Crima si pedeapsa' , 'Cartex 2000' , 2007 , 527 , 4 ,'Romana' , 821.16) , 
('978-973-689-759-7' , 'Adulter' , 'Humanitas Fiction' , 2014 , 255 , 3, 'Romana' , 821.134) ,
('973-50-0243-4' , 'Alchimistul' , 'Humanitas' , 2002 , 192 , 3 , 'Romana' , 821.134) , 
('978-973-46-0825-6' , 'Batranul si marea' , 'Polirom' , 2007 , 196 , 4 , 'Romana' , 821.111) , 
('978-973-46-1387-8' , 'Gradina raiului' , 'Polirom' , 2009 , 237 , 2 , 'Romana' , 821.111) ,
('973-9139-95-7' , 'Insule in deriva' , 'Vivaldi' , 1998 , 544 , 3 , 'Romana' , 821.111) , 
('978-606-579-211-1' , 'Urzeala tronurilor' , 'Nemira' , 2011 , 904 , 2 , 'Romana' , 821.111) , 
('978-606-579-236-4' , 'Inclestarea regilor' , 'Nemira' , 2011 , 1040 , 2 , 'Romana' , 821.111) , 
('978-606-579-245-6' , 'Iuresul sabiilor' , 'Nemira' , 2011 , 1312 , 2 , 'Romana' , 821.111) , 
('973-583-287-9' , 'Harry Potter si Camera Secretelor' , 'Egmont' , 2001 , 299 , 3 , 'Romana' , 821.111) , 
('973-583-301-8' , 'Harry Potter , prizonier la Azkaban' , 'Egmont' , 2001 , 309 , 3 , 'Romana' , 821.111) , 
('973-608-805-7' , 'Cele mai frumoase pagini' , 'Coresii' , 2006 , 119 , 2 , 'Romana' , 821.135 ) ,
('978-973-50-1137-6' , '11 elegii' , 'Humanitas' , 2007 , 83 , 2 , 'Romana' , 821.135) , 
('973-9155-005-9' , 'O scrisoare pierduta' , 'Imprimeria Ardealului' , 1996 , 128 , 1 , 'Romana' , 821.135);




select * from gen;
insert into carti_gen values 
('973-98761-1-0' , 6) , 
('978-973-104-069-1' , 6) , 
('978-973-689-759-7' , 3) , 
('973-50-0243-4' , 3) , 
('978-973-46-0825-6' , 6) ,
('978-973-46-1387-8' , 6) ,
('973-9139-95-7' , 6) , 
('978-606-579-211-1' , 1) ,
('978-606-579-236-4' , 1) ,
('978-606-579-245-6' , 1) , 
('973-583-287-9' , 8) , 
('973-583-301-8' , 8) ,
('973-608-805-7' , 2) ,
('978-973-50-1137-6' , 2) ,
('973-9155-005-9' , 5);


insert into carti_autor values 
('973-98761-1-0' , 7) , 
('978-973-104-069-1' , 7) ,
('978-973-689-759-7' , 8) , 
('973-50-0243-4' , 8) ,
('978-973-46-0825-6' , 9) ,
('978-973-46-1387-8' , 9) ,
('973-9139-95-7' , 9) , 
('978-606-579-211-1' , 10) ,
('978-606-579-236-4' , 10) ,
('978-606-579-245-6' , 10) ,
('973-583-287-9' , 11) ,
('973-583-301-8' , 11) ,
('973-608-805-7' , 12) ,
('978-973-50-1137-6' , 13) ,
('973-9155-005-9' , 14);



insert into cod_carti values
(821.134 , 'Literatura portugheza şs de limba portugheza') ,
(821.135 , 'Literatura romana');

insert into user (cont , parola) values
('grec_mihai96@yahoo.com' , '1996') ,
('dodo96@gmail.com' , '1996') ,
('mariussupuran@yahoo.com' , '1996') , 
('tudorpalade2006@yahoo.com' , '1996') ,
('iuliagc@gmail.com' , '1996') ,
('iuliaserban@yahoo.com' , '1996') ,
('mariacurtean97@gmail.com' , '1996') ,
('oana.marincas@gmail.com' ,'1996' ) ,
('admin' , 'admin');

