#vederi

#1 Tabela cu cei care trebuie sa plateasca amenda
create view AfisarePenalizati as 
select membru.nume , membru.prenume , membru.email , membru.nrtelefon , penalizari.NrZileIntarziate from membru
join penalizari on penalizari.idMembru = membru.idMembru
join validitateimprumut on validitateimprumut.idMembru = membru.idMembru
where validitateimprumut.dreptImprumut = 0
with check option;

#2Carti cu numele , autorul , genul cartii si descrierea acesteia + numarul de copiii disponibile
create view Carte as
select carti.titlu , autor.Nume , autor.Prenume , gen.Gencol , cod_carti.Descriere , carti.NrCopiiDisponibile from carti
join carti_autor on carti.Isbn = carti_autor.Isbn
join autor on carti_autor.idAutor = autor.idAutor
join carti_gen on carti_gen.Isbn = carti.Isbn
join gen on carti_gen.idGen = gen.idGen
join cod_carti on cod_carti.Cod = carti.Cod
with check option;

#3Numele , prenumele , cartea imprumutate si termenii de primire-predare a persoanelor ce au momentan carti imprumutate
create view imprumut as 
select membru.nume , membru.Prenume , imprumuturicurente.DataImprumutului , imprumuturicurente.DataValabilitate , carti.titlu 
from membru
join imprumuturicurente on imprumuturicurente.idMembru = membru.idMembru
join carti on carti.Isbn = imprumuturicurente.Isbn
order by membru.Nume , membru.Prenume asc
with check option;

#4 Informatii complete despre Membrii , vederea va fi folosita pentru a modifica daca e necesar datele din tabelele Membru sau Adresa
#in caz ca ceva s'a modificat(se verifica la inceputul anului cand se vizeaza legitimatia)

create view MembruComplet as
select membru.Nume , membru.Prenume , membru.Email , membru.NrTelefon , adresa.Strada , adresa.Numar , adresa.Bloc , adresa.Apartament
from membru join adresa on membru.idMembru = adresa.idMembru with check option;



