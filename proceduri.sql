#proceduri

#1 Adaugare user
drop procedure if exists creare_user;
delimiter //
create procedure creare_user(p_cont varchar(45) , p_parola varchar(30) )

begin

insert into user (cont , parola )
values (p_cont , p_parola );

end//
delimiter ;


#2 Adaugare penalizare , in momentul depasirii celor 2 saptamani , daca nu se face prelungire in timp util se plateste 1 leu/zi
#si se revoca dreptul de a lua imprumuta carti
drop procedure if exists adaugarePenalizare;
delimiter //
create procedure adaugarePenalizare(p_nume varchar(20) , p_prenume varchar(20))
begin
set @id = null;
set @dat = null;
set @zileintarziate = 0;
select @id:=membru.idMembru from membru where nume = p_nume and prenume = p_prenume;
select @dat:=datavalabilitate from imprumuturicurente where imprumuturicurente.idmembru = @id order by datavalabilitate limit 1;
select @zileintarziate:=datediff(curdate() , @dat);
if (@zileintarziate > 0) then
update validitateimprumut set dreptimprumut = 0 where validitateimprumut.idmembru = @id;
update penalizari set nrzileintarziate = @zileintarziate where penalizari.idmembru = @id;
end if;
end//
delimiter ;
#3 Prelungire carte , trebuie tinut minte numarul de prelungiri pt o carte , maxim 2 consecutiv
drop procedure if exists prelungireCarte;
delimiter //
create procedure prelungireCarte(p_nume varchar(20) , p_prenume varchar(20) , p_titlu varchar(45))
begin
declare id int default null;
declare copieIsbn varchar(30) default null;
declare nrzile int;
select membru.idMembru into id from membru where nume = p_nume and prenume = p_prenume;
select carti.Isbn into copieIsbn from carti where p_titlu = titlu;
if (id is not null and copieIsbn is not null) then
select datediff(datavalabilitate , dataimprumutului) into nrzile from imprumuturicurente 
where imprumuturicurente.idMembru = id and imprumuturicurente.isbn = copieisbn;
if (nrzile <= 28) then
update imprumuturicurente set datavalabilitate = date_add(curdate() , interval 14 day);
end if;
end if;
end//
delimiter ;

#4 Imprumut carte , se adauga datele necesare si se tine seama de numarul de carti imprumutate , maxim 3
drop procedure if exists imprumutCarte;
delimiter //
create procedure imprumutCarte(p_nume varchar(20) , p_prenume varchar(20) , p_titlu varchar(45))
begin
declare id int default null;
declare copieIsbn varchar(30) default null;
declare nrimprumuturi int default 0;
declare nrcopii int default 0;
declare bul int default 0;
select membru.idMembru into id from membru where nume = p_nume and prenume = p_prenume;
select carti.Isbn into copieIsbn from carti where p_titlu = titlu;
select dreptimprumut into bul from validitateimprumut where idmembru = id;
if (id is not null and copieIsbn is not null and bul = 1) then
select count(imprumuturicurente.isbn) into nrimprumuturi from imprumuturicurente where imprumuturicurente.idmembru = id 
group by imprumuturicurente.idmembru;
if (nrimprumuturi < 3) then
select nrcopiidisponibile into nrcopii from carti where carti.isbn = copieIsbn;
if (nrcopii > 0) then
insert into imprumuturicurente values (id , copieisbn , curdate() , date_add(curdate() , interval 14 day));
update carti set nrcopiidisponibile = nrcopiidisponibile - 1 where carti.isbn = copieIsbn;
delete from rezervariinternet where rezervariinternet.idMembru = id and rezervariinternet.Isbn = copieIsbn;
end if;
end if;
end if;
end//
delimiter ;
#5 Rezervare carte online , valabila 5 zile , maxim 3 carti rezervate
drop procedure if exists rezervareCarte;
delimiter //
create procedure rezervareCarte(p_nume varchar(20) , p_prenume varchar(20) , p_titlu varchar(45))
begin
declare id int default null;
declare copieIsbn varchar(30) default null;
declare nrimprumuturi int default 0;
declare nrcopii int default 0;
declare bul int default 0;
select membru.idMembru into id from membru where nume = p_nume and prenume = p_prenume;
select carti.Isbn into copieIsbn from carti where p_titlu = titlu;
select dreptimprumut into bul from validitateimprumut where idmembru = id;
if (id is not null and copieIsbn is not null and bul = 1) then
select count(rezervariinternet.isbn) into nrimprumuturi from rezervariinternet where rezervariinternet.idmembru = id 
group by rezervariinternet.idmembru;
if (nrimprumuturi < 3) then
select nrcopiidisponibile into nrcopii from carti where carti.isbn = copieIsbn;
if (nrcopii > 0) then
insert into rezervariinternet values (id , copieisbn , curdate() , date_add(curdate() , interval 5 day));
update carti set nrcopiidisponibile = nrcopiidisponibile - 1 where carti.isbn = copieIsbn;
end if;
end if;
end if;
end//
delimiter ;
#6 Returnare carte
drop procedure if exists returnareCarte;
delimiter //
create procedure returnareCarte(p_nume varchar(20) , p_prenume varchar(20) , p_titlu varchar(45))
begin
declare id int default null;
declare copieIsbn varchar(30) default null;
declare inceput date;
declare sfarsit date;
select membru.idMembru into id from membru where nume = p_nume and prenume = p_prenume;
select carti.Isbn into copieIsbn from carti where p_titlu = titlu;
if (id is not null and copieIsbn is not null) then

select dataimprumutului into inceput from imprumuturicurente
join membru on imprumuturicurente.idmembru = membru.idmembru
join carti on imprumuturicurente.isbn = carti.isbn
where membru.idmembru = id and carti.isbn = copieisbn;

select datavalabilitate into sfarsit from imprumuturicurente
join membru on imprumuturicurente.idmembru = membru.idmembru
join carti on imprumuturicurente.isbn = carti.isbn
where membru.idmembru = id and carti.isbn = copieisbn;

delete from imprumuturicurente where idmembru = id and isbn = copieisbn;
insert into istoricimprumuturi values(id , inceput , sfarsit , copieisbn);
update carti set NrCopiiDisponibile = NrCopiiDisponibile + 1 where carti.Isbn = copieisbn;
end if;
end//
delimiter ;

#7 Platire amenda
drop procedure if exists platireAmenda;
delimiter //
create procedure platireAmenda (p_nume varchar(20) , p_prenume varchar(20))
begin
declare id int default null;
select membru.idMembru into id from membru where nume = p_nume and prenume = p_prenume;
if (id is not null) then
update validitateimprumut set dreptimprumut = 1 where validitateimprumut.idMembru = id;
update penalizari set nrzileintarziate = 0 where penalizari.idMembru = id;
end if;
end//
delimiter ;
#8Stergere rezervari;in fiecare zi se vor verifica rezervari si se vor sterge cele expirate , de asemenea se va incrementa nr de 
#copii disponibile pt acele carti
drop procedure if exists stergereRezervari;
delimiter //
create procedure stergereRezervari()
begin
update carti join rezervariinternet on carti.isbn = rezervariinternet.isbn
set NrCopiiDisponibile = NrCopiiDisponibile + 1 where datediff(curdate() , datavalabilitate) > 0;
delete from rezervariinternet where datediff(curdate() , datavalabilitate) > 0;
end//
delimiter ;
 
#9 Afisare istoric imprumut
drop procedure if exists afisareIstoric;
delimiter //
create procedure afisareIstoric (p_nume varchar(20) , p_prenume varchar(20))
begin

declare id int default null;
select membru.idMembru into id from membru where nume = p_nume and prenume = p_prenume;
if (id is not null) then
select carti.Titlu , istoricimprumuturi.DataImprumutului , istoricimprumuturi.DataReturnarii from istoricimprumuturi
join carti on carti.Isbn = istoricimprumuturi.Isbn
join membru on membru.idMembru = istoricimprumuturi.idMembru
where membru.idMembru = id
order by istoricimprumuturi.DataImprumutului , istoricimprumuturi.DataReturnarii;
end if;
end//

delimiter ;
